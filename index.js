const express = require("express");
const mongoose = require("mongoose");

const app = express();
app.use(express.json());

const port = 8000;

const {userRouter} = require("./app/routes/userRoute");
const {carRouter} = require("./app/routes/carRoute");

const dbName = "CRUD_512_40";
mongoose.connect("mongodb://localhost:27017/"+dbName,(error)=>{
    if(error) throw error;
    else console.log("Successfull connect to DB: " + dbName);
});

app.get("/",(req, res)=>{
   return res.status(200).json({
    message: "test"
   })
});

app.use("/", userRouter);
app.use("/", carRouter);

app.listen(port,()=>{
    console.log("Connect to port: "+port);
})