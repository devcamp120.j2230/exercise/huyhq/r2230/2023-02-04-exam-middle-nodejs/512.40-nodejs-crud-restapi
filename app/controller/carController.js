const { default: mongoose, model } = require("mongoose");

const carModel = require("../model/Car");
const { populate } = require("../model/User");
const userModel = require("../model/User");

const createCarOfUser = (req, res) => {
    let userId = req.params.userId;

    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            mesage: `User Id khong dung!`
        });
    };

    let body = req.body;

    if (!body.model) {
        return res.status(400).json({
            mesage: "Error 400: Model Car phai co!"
        });
    };

    if (!body.vld) {
        return res.status(400).json({
            mesage: "Error 400: VLD Car phai co!"
        });
    };

    const newCar = carModel({
        _id: mongoose.Types.ObjectId(),
        model: body.model,
        vld: body.vld
    })

    carModel.create(newCar, (error, data) => {
        if (error) {
            return res.status(500).json({
                mesage: `Error 500: ${error.mesage}`
            });
        } else {
            userModel.findByIdAndUpdate(userId,
                {
                    $push: { cars: data._id }
                },
                (err) => {
                    if (err) {
                        return res.status(500).json({
                            mesage: `Error 500: ${err.mesage}`
                        });
                    } else {
                        return res.status(201).json({
                            mesage: `Tao moi thanh cong!`,
                            car: data
                        });
                    }
                }
            )
        }
    })
};

const getAllCarOfUser = (req, res) => {
    let userId = req.params.userId;

    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            mesage: `User Id khong dung!`
        });
    };

    userModel.findById(userId)
        .populate("cars")
        .exec((err, data) => {
            if (err) {
                return res.status(500).json({
                    mesage: `Error 500: ${err.mesage}`
                });
            } else {
                return res.status(200).json({
                    mesage: `Lay du lieu thanh cong!`,
                    car: data.cars
                });
            }
        })
};

const getCarById = (req, res) => {
    let carId = req.params.carId;

    if (!mongoose.Types.ObjectId.isValid(carId)) {
        return res.status(400).json({
            mesage: `Car Id khong dung!`
        });
    };

    carModel.findById(carId, (err, data) => {
        if (err) {
            return res.status(500).json({
                mesage: `Error 500: ${err.mesage}`
            });
        } else {
            return res.status(200).json({
                mesage: `Lay du lieu thanh cong!`,
                car: data
            });
        }
    })
};

const editCarById = (req, res) => {
    let carId = req.params.carId;

    if (!mongoose.Types.ObjectId.isValid(carId)) {
        return res.status(400).json({
            mesage: `Car Id khong dung!`
        });
    };

    let body = req.body;

    if (!body.model) {
        return res.status(400).json({
            mesage: "Error 400: Model Car phai co!"
        });
    };

    if (!body.vld) {
        return res.status(400).json({
            mesage: "Error 400: VLD Car phai co!"
        });
    };

    const car = carModel({
        model: body.model,
        vld: body.vld
    })

    carModel.findByIdAndUpdate(carId, car, (err, data) => {
        if (err) {
            return res.status(500).json({
                mesage: `Error 500: ${err.mesage}`
            });
        } else {
            return res.status(200).json({
                mesage: `Sua du lieu thanh cong!`,
                car: data
            });
        }
    })
};

const deleteCarById = (req, res) => {
    let userId = req.params.userId;

    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            mesage: `User Id khong dung!`
        });
    };

    let carId = req.params.carId;

    if (!mongoose.Types.ObjectId.isValid(carId)) {
        return res.status(400).json({
            mesage: `Car Id khong dung!`
        });
    };

    carModel.findByIdAndDelete(carId, (error, data) => {
        if (error) {
            return res.status(500).json({
                mesage: `Error 500: ${err.mesage}`
            });
        } else {
            userModel.findByIdAndUpdate(userId,
                {
                    $pull: { cars: carId },
                },
                (err) => {
                    if (err) {
                        return res.status(500).json({
                            mesage: `Error 500: ${err.mesage}`
                        });
                    } else {
                        return res.status(204).json({
                            mesage: `Xoa thanh cong!`,
                            car: data
                        });
                    }
                }
            )
        }
    })
};

module.exports = {
    createCarOfUser,
    getAllCarOfUser,
    getCarById,
    editCarById,
    deleteCarById
}