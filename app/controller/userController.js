const {default: mongoose} = require("mongoose");

const userModel = require("../model/User");

const createNewUser = (req, res)=>{
    let body = req.body;
    if(!body.phone){
        return res.status(400).json({
            mesage: "Error 400: So dien thoai phai co!"
        });
    };

    if((!Number.isInteger(body.age) || body.age < 0) && body.age){
        return res.status(400).json({
            mesage: "Error 400: Tuoi khong dung dinh dang!"
        });
    };

    const newUser = userModel({
        _id: mongoose.Types.ObjectId(),
        phone: body.phone,
        age: body.age
    });

    userModel.create(newUser, (error, data)=>{
        if(error){
            return res.status(500).json({
                mesage: `Error 500: ${error.mesage}!`
            });
        }else{
            return res.status(201).json({
                mesage: `Tao moi User thanh cong!`,
                user: data
            });
        }
    })
};

const getAllUser = (req, res)=>{
    userModel.find((error, data)=>{
        if(error){
            return res.status(500).json({
                mesage: `Error 500: ${error.mesage}!`
            });
        }else{
            return res.status(200).json({
                mesage: `Lay du lieu thanh cong!`,
                user: data
            });
        }
    })
};

const getUserById = (req, res)=>{
    let id = req.params.id;
    if(!mongoose.Types.ObjectId.isValid(id)){
        return res.status(400).json({
            mesage: `User Id khong dung!`
        });
    }

    userModel.findById(id, (error, data)=>{
        if(error){
            return res.status(500).json({
                mesage: `Error 500: ${error.mesage}!`
            });
        }else{
            return res.status(200).json({
                mesage: `Lay du lieu thanh cong!`,
                user: data
            });
        }
    })
};

const editUserById = (req, res)=>{
    let id = req.params.id;
    if(!mongoose.Types.ObjectId.isValid(id)){
        return res.status(400).json({
            mesage: `User Id khong dung!`
        });
    }
    let body = req.body;

    if(!body.phone){
        return res.status(400).json({
            mesage: "Error 400: So dien thoai phai co!"
        });
    };

    if((!Number.isInteger(body.age) || body.age < 0) && body.age){
        return res.status(400).json({
            mesage: "Error 400: Tuoi khong dung dinh dang!"
        });
    };

    const userEdit = userModel({
        phone: body.phone,
        age: body.age
    });

    userModel.findByIdAndUpdate(id, userEdit, (error, data)=>{
        if(error){
            return res.status(500).json({
                mesage: `Error 500: ${error.mesage}!`,
            });
        }else{
            return res.status(200).json({
                mesage: `Sua thanh cong User id: ${id}`,
                user: data
            });
        }
    })
};

const deleteUserById = (req, res)=>{
    let id = req.params.id;
    if(!mongoose.Types.ObjectId.isValid(id)){
        return res.status(400).json({
            mesage: `User Id khong dung!`
        });
    }

    userModel.findByIdAndDelete(id, (error, data)=>{
        if(error){
            return res.status(500).json({
                mesage: `Error 500: ${error.mesage}!`
            });
        }else{
            return res.status(204).json({
                mesage: `Xoa thanh cong User id: ${id}`,
                user: data
            });
        }
    })
};

module.exports = {
    createNewUser,
    getAllUser,
    getUserById,
    editUserById,
    deleteUserById
}