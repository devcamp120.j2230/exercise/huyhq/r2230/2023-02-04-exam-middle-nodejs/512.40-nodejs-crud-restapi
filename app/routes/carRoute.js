const express = require("express");

const carRouter = express.Router();

const {
    createCarOfUser,
    getAllCarOfUser,
    getCarById,
    editCarById,
    deleteCarById
} = require("../controller/carController");

carRouter.get("/user/:userId/car",getAllCarOfUser);
carRouter.post("/user/:userId/car",createCarOfUser);
carRouter.get("/car/:carId",getCarById);
carRouter.put("/car/:carId",editCarById);
carRouter.delete("/user/:userId/car/:carId", deleteCarById);

module.exports = {carRouter};