const express = require("express");

const userRouter = express.Router();

const {
    createNewUser,
    getAllUser,
    getUserById,
    editUserById,
    deleteUserById
} = require("../controller/userController");

userRouter.get("/user",getAllUser);
userRouter.post("/user",createNewUser);
userRouter.get("/user/:id",getUserById);
userRouter.put("/user/:id",editUserById);
userRouter.delete("/user/:id",deleteUserById);

module.exports = {userRouter};