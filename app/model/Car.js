const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const carSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    model: {
        type: String, 
        required: true, 
    },
	vld: {
        type: String, 
        required: true, 
        unique: true
    },
})

module.exports = mongoose.model("car", carSchema);
