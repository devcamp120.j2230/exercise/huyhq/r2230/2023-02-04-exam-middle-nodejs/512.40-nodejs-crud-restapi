const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const userSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    phone: {
        type: String, 
        required: true, 
        unique: true
    },
	age: {
        type: Number, 
        default: 0
    },
	cars: [{
        type: mongoose.Types.ObjectId,
        ref: "car"
    }]
})

module.exports = mongoose.model("user", userSchema);
